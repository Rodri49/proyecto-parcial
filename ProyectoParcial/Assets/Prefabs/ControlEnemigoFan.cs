using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoFan : MonoBehaviour
{ 
    private int da�o;
    private GameObject jugador;
    public int rapidez;
    
    void Start() 
    { 
        da�o = 15;
        jugador = GameObject.Find("Jugador");
    } 

    private void Inflingirda�o() 
    {
        jugador.GetComponent<ControlJugador>().hp -= da�o;
        Debug.Log("a");
    }
    
    private void OnCollisionEnter(Collision collision) 
    {
        if (collision.gameObject.CompareTag("enemigofan")) 
        {
            Inflingirda�o();
        }
    }

    private void Update() 
    { 
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    } 
}