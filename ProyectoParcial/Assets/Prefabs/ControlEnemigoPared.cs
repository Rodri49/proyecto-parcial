using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoPared : MonoBehaviour

{ 
    bool tengoQueirAtras = false; 
    public int rapidez;

    void Update() 
    { 
        if (transform.position.z >= 16) 
        {
            tengoQueirAtras = true; 
        } if (transform.position.z <= -15)
        
        { 
            tengoQueirAtras = false;
        }
        
        if (tengoQueirAtras) 
        { 
            Atras(); 
        } 
        
        else 
        { 
            Adelante();
        } 
    } 
    
    void Adelante() 
    { 
        transform.position += transform.forward * rapidez * Time.deltaTime; 
    
    } 
    
    void Atras() 
    { 
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    } 
}