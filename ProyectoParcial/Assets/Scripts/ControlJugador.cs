using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int hp;
    public int rapidez;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        hp = 100;
        col = GetComponent<CapsuleCollider>();
    }

    public void RecibirDaņo()
    {
        hp = hp - 15;

        if (hp >= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer() 
    { 
        Destroy(gameObject); 
    }

    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }

    private void OnTriggerEnter(Collider other)
    { 
        if (other.gameObject.CompareTag("coleccionable") == true) 
        { 
            other.gameObject.SetActive(false); 
        } 
    }

    private bool EstaEnPiso() 
    { 
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso); 
    }
}