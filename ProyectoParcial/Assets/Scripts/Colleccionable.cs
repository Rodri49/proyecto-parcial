using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colleccionable : MonoBehaviour
{
    public float factorRedimensionamiento = 0.5f;
    public float factorVelocidad = 2.0f;

    void OnTriggerEnter(Collider other)
    {
            
       if (other.gameObject.CompareTag("Jugador"))
       {
         GetComponent<Transform>().localScale *= factorRedimensionamiento;
         GetComponent<Rigidbody>().velocity *= factorVelocidad;
         Destroy(gameObject);
       }
    }
}
